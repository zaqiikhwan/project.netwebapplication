using MSSQL.Models;

namespace MSSQL.Interfaces
{
    public interface ITokenService
    {
        string CreateToken(User user);
    }
}