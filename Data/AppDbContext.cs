using Microsoft.EntityFrameworkCore;
using MSSQL.Models;

namespace MSSQL.Data
{
    public class AppDbContext(IConfiguration configuration) : DbContext 
    {
        public IConfiguration Config { get; set; } = configuration;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Config.GetConnectionString("DefaultConnection"));
        }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //Seed Departments Table
            modelBuilder.Entity<Department>().HasData(
                new Department { DepartmentId = 1, DepartmentName = "Backend" });
            modelBuilder.Entity<Department>().HasData(
                new Department { DepartmentId = 2, DepartmentName = "Devops" });
            modelBuilder.Entity<Department>().HasData(
                new Department { DepartmentId = 3, DepartmentName = "Mobile" });
            modelBuilder.Entity<Department>().HasData(
                new Department { DepartmentId = 4, DepartmentName = "Frontend" });

            // Seed Employees Table
            modelBuilder.Entity<Employee>().HasData(
                new Employee {EmployeeId = 1231,Name = "Tengku", DepartmentId = 1 });
            modelBuilder.Entity<Employee>().HasData(
                new Employee {EmployeeId = 1232, Name = "Alby", DepartmentId = 2 });
            modelBuilder.Entity<Employee>().HasData(
                new Employee {EmployeeId = 1233, Name = "Yogi", DepartmentId = 3 });
            modelBuilder.Entity<Employee>().HasData(
                new Employee {EmployeeId = 1234, Name = "Ariz", DepartmentId = 4});

        }


    }
}
