using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MSSQL.Data;
using MSSQL.Interfaces;
using MSSQL.Models;
using MSSQL.Models.Dto;

namespace MSSQL.Controllers;

public class UserController(AppDbContext context, ITokenService tokenService) : Controller
{
    private readonly AppDbContext _context = context;
    private readonly ITokenService _tokenService = tokenService;

    [HttpPost("api/v1/register")]
    public async Task<ActionResult<User>> Register([FromBody] UserRegisterDto registerDto)
    {
        if (await UserExists(registerDto.Username)) return BadRequest("userName is already exist, please change it.");
        var hmac = new HMACSHA512();

        var newUser = new User
        {
            Username = registerDto.Username.ToLower(),
            PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(registerDto.Password)),
            PasswordSalt = hmac.Key
        };

        _context.Add(newUser);
        await _context.SaveChangesAsync();
        return newUser;
    }


    [HttpPost("api/v1/login")]
    public async Task<ActionResult<UserProfileDto>> Login([FromBody] UserLoginDto loginDto)
    {
        var user = await _context.Users.SingleOrDefaultAsync(x => x.Username == loginDto.Username);

        if (user == null) return Unauthorized("Invalid UserName");

        var hmac = new HMACSHA512(user.PasswordSalt);

        var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(loginDto.Password));

        for (int i = 0; i < computedHash.Length; i++)
        {
            if (computedHash[i] != user.PasswordHash[i]) return Unauthorized("Invalid Password");
        }

        return new UserProfileDto {
            Username = user.Username,
            Token = _tokenService.CreateToken(user)
        };
    }

    // get all users
    [Route("api/v1/users")]
    [Authorize]
    public async Task<JsonResult> GetAll()
    {
        return new JsonResult(await _context.Users.ToListAsync());
    }

    // update profile user
    [HttpPatch("api/v1/profile")]
    [Authorize]
    public async Task<ActionResult<User>> UpdateProfile([FromBody] UserUpdateDto userUpdateDto)
    {

        var username = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
        var currentUser = await _context.Users.SingleOrDefaultAsync(x => x.Username == username);
        if (currentUser == null) return NotFound("User not found");

        currentUser.Name = userUpdateDto.Name ?? currentUser.Name;
        currentUser.Username = userUpdateDto.Username ?? currentUser.Username;
        currentUser.PasswordHash = userUpdateDto.Password != null ? new HMACSHA512().ComputeHash(Encoding.UTF8.GetBytes(userUpdateDto.Password)) : currentUser.PasswordHash;
        currentUser.UpdatedAt = DateTime.Now;

        await _context.SaveChangesAsync();
        return currentUser;
    }

    [HttpGet("api/v1/profile")]
    [Authorize]
    public async Task<ActionResult<User>> GetProfile()
    {
        var username = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
        var user = await _context.Users.SingleOrDefaultAsync(x => x.Username == username);
        if (user == null) return NotFound("User not found");
        return user;
    }
    private async Task<bool> UserExists(string username)
    {
        return await _context.Users.AnyAsync(x => x.Username == username.ToLower());
    }
}