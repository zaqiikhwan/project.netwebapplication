
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MSSQL.Data;

namespace MSSQL.Controllers
{
    public class EmployeeController(AppDbContext context) : Controller
    {
        private readonly AppDbContext _context = context;

        public async Task<IActionResult> Index()
        {
            return View(await _context.Employees.ToListAsync());
        }

        [Route("api/employees")]
        public async Task<JsonResult> GetAll()
        {
            return new JsonResult(await _context.Employees.ToListAsync());
        }

        [Route("api/employee/{id}")]
        public async Task<JsonResult> GetById(int id)
        {
            return new JsonResult(await _context.Employees.FindAsync(id));
        }

        [Route("api/employee")]
        [HttpPost]
        public async Task<JsonResult> CreateEmp([FromBody] Employee employee)
        {
            try
            {
                _context.Add(employee);
                await _context.SaveChangesAsync();
                return new JsonResult(employee);
            }
            catch (Exception e)
            {
                return new JsonResult(new { success = false, message = e.InnerException });
            }
        }

        [Route("api/employee/{id}")]
        [HttpPatch]

        public async Task<JsonResult> UpdateEmp([FromBody] Employee employee, int id)
        {
            try
            {
                var existingEmployee = await _context.Employees.FindAsync(id);

                if (existingEmployee == null)
                {
                    return new JsonResult(new { success = false, message = "Employee not found" });
                }

                existingEmployee.Name = employee.Name;
                existingEmployee.DepartmentId = employee.DepartmentId;

                _context.Entry(existingEmployee).State = EntityState.Modified;
                _context.Update(existingEmployee);
                await _context.SaveChangesAsync();

                return new JsonResult(existingEmployee);
            }
            catch (Exception e)
            {
                return new JsonResult(new { success = false, message = e.Message });
            }
        }

        [Route("api/employee/{id}")]
        [HttpDelete]
        public async Task<JsonResult> DeleteEmp(int id)
        {
            try
            {
                var employee = await _context.Employees.FindAsync(id);

                if (employee == null)
                {
                    return new JsonResult(new { success = false, message = "Error while deleting" });
                }

                if (employee != null) _context.Employees.Remove(employee);
                await _context.SaveChangesAsync();
                return new JsonResult(employee);
            }
            catch (Exception e)
            {
                return new JsonResult(new { success = false, message = e.Message });
            }
        }
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees
                .FirstOrDefaultAsync(m => m.EmployeeId == id);
            if (employee == null)
            {
                return NotFound();
            }

            return View(employee);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EmployeeId,Name,DepartmentId")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                _context.Add(employee);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(employee);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }
            return View(employee);
        }

        [HttpPatch]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Name")] Employee employee)
        {
            if (id != employee.EmployeeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(employee);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployeeExists(employee.EmployeeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(employee);
        }

        private bool EmployeeExists(int employeeId)
        {
            return (_context.Employees?.Any(e => e.EmployeeId == employeeId)).GetValueOrDefault();
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(int id)
        {
            try
            {
                var employee = await _context.Employees.FindAsync(id);

                if (employee == null)
                {
                    return Json(new { success = false, message = "Error while deleting" });
                }

                if (employee != null) _context.Employees.Remove(employee);
                await _context.SaveChangesAsync();
                return new JsonResult(employee);
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message });
            }
        }
    }
}