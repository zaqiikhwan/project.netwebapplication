using System.ComponentModel.DataAnnotations;

namespace MSSQL.Models
{
    public class User
    {
        public Guid UserId { get; set; }
        public string? Name { get; set; }
        
        public required string Username { get; set; }
        
        public required byte[]  PasswordHash { get; set; }
        public required byte[] PasswordSalt { get; set; }
        public string? Role { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; }
    }
}