namespace MSSQL.Data
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string? Name { get; set; }
        public int? DepartmentId { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;

        public DateTime UpdatedAt { get; set; } 
    }
}
