namespace MSSQL.Models.Dto
{
    public class UserRegisterDto
    {
        public required string Username { get; set; }
        public required string Password { get; set; }
    }

    public class UserLoginDto
    {
        public required string Username { get; set; }
        public required string Password { get; set; }
    }

    public class UserProfileDto
    {
        public required string Username { get; set; }
        public required string Token { get; set; }
    }

    public class UserUpdateDto
    {
        public string? Name { get; set; }
        public string? Username { get; set; }
        public string? Password { get; set; }
    }

}